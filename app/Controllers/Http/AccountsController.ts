import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Response from 'App/Helpers/Response'
import User from 'App/Models/User'
import AuthValidator from 'App/Validators/AuthValidator'

export default class AuthenticationController {
    public async register({request,response}: HttpContextContract){
        await request.validate(AuthValidator)
        const apiResponse = new Response(response)
        const data = request.only([
            "email",
            "username",
            "password"
            
        ])

        const emailChecker = await User.query()
        .where('email', data.email)
        .first()

        if(!emailChecker){
            await User.create({
                email:data.email,
                username: data.username,
                password: data.password
            })
            return apiResponse.data("Account Created Successfully: Welcome " + data.username)  

        } else {
            return apiResponse.badRequest("Email already existed! Please choose another email")
        }   
    }

    public async login({auth,request,response}: HttpContextContract){
        const apiResponse = new Response(response)
        const data = request.only([
            "email",
            "password"
        ])

        //Authenticate User!
        try {
            const authentication = await auth.use('api').attempt(data.email,data.password)
            const user = await User.query()
            .where("id", authentication.user.id)
            .first()


            if (!user) {
                return apiResponse.notFound(
                  "You don't have an account, please sign up first."
                );
              }

              return apiResponse.data("You Successfully Logged In! " + data.email)
        
        } catch(error){
            //Error to check if Input is Incorrect
            const emailNotFoundMsg = "E_INVALID_AUTH_UID: User not found"
            const passwordMisMatchMsg = "E_INVALID_AUTH_PASSWORD: Password mis-match"

            if (error.message == passwordMisMatchMsg) {
                return apiResponse.badRequest("Incorrect Password.")
              }
        
              if (error.message == emailNotFoundMsg) {
                return apiResponse.badRequest("You don't have an account, please sign up first.")
              }
        
              console.log(error)
              return apiResponse.badRequest("Invalid email or password.")
            }
        }
    }

